# Examples

The following command to run the helloworld demo:

protoc -I ./proto    --go_out ./proto --go_opt paths=source_relative    --go-grpc_out ./proto --go-grpc_opt paths=source_relative    ./proto/helloworld/helloworld.proto 

protoc -I ./proto  -I ~/Projects/go/protoc-gen-validate  --go_out="./proto/"  --go_opt paths=source_relative    --go-grpc_out ./proto --go-grpc_opt paths=source_relative   --proto_path=. --validate_out="lang=go:."  ./proto/helloworld/helloworld.proto

protoc -I ./proto  -I ~/Projects/go/protoc-gen-validate --go_out ./proto --go_opt paths=source_relative   --go-grpc_out ./proto --go-grpc_opt paths=source_relative   --grpc-gateway_out ./proto --grpc-gateway_opt paths=source_relative   ./proto/helloworld/helloworld.proto